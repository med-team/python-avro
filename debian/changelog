python-avro (1.12.0+dfsg-1) unstable; urgency=medium

  * Switch to autopkgtest-pkg-pybuild.
  * New upstream release.

 -- Colin Watson <cjwatson@debian.org>  Thu, 15 Aug 2024 17:06:35 +0100

python-avro (1.11.3+dfsg-1) unstable; urgency=medium

  * Team upload.
  * d/copyright: Add lang/rust/* to Files-Excluded.
  * New upstream release.

 -- Colin Watson <cjwatson@debian.org>  Tue, 30 Apr 2024 18:09:36 +0100

python-avro (1.11.1+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Cherry-pick upstream fix for Python 3.11 tests (Closes: #1026605)

 -- Scott Talbert <swt@techie.net>  Thu, 29 Dec 2022 17:00:27 -0500

python-avro (1.11.1+dfsg-1) unstable; urgency=medium

  * Team upload.
  * Fix watch file
  * Standards-Version: 4.6.1 (routine-update)
  * Build-Depends: pybuild-plugin-pyproject
    Closes: #1020129
  * Lintian-overrides for false positives

 -- Andreas Tille <tille@debian.org>  Tue, 27 Sep 2022 11:13:17 +0200

python-avro (1.11.0+dfsg-1) unstable; urgency=medium

  * Team upload.
  * Move package to Debian Python Team
  * Standards-Version: 4.6.0 (routine-update)
  * No tab in license text (routine-update)
  * Remove unneeded patch
  * Fix sourcedir
  * Fix clean target
  * Skip test accessing network

 -- Andreas Tille <tille@debian.org>  Thu, 17 Feb 2022 16:19:01 +0100

python-avro (1.10.2+dfsg-1) unstable; urgency=medium

  * Team Upload.
  * New upstream version 1.10.2+dfsg
  * Refresh patch
  * Bump watch file version to 4

 -- Nilesh Patra <nilesh@debian.org>  Sun, 15 Aug 2021 17:43:06 +0530

python-avro (1.10.1+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.5.1 (routine-update)

 -- Nilesh Patra <npatra974@gmail.com>  Tue, 08 Dec 2020 01:27:43 +0530

python-avro (1.10.0+dfsg-1) unstable; urgency=medium

  * Team Upload.
  * New upstream version 1.10.0+dfsg
  * Disable linting
  * Update Build-Dep
  * Don't install VERSION.txt
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Bug-Database, Repository, Repository-Browse.

 -- Nilesh Patra <npatra974@gmail.com>  Tue, 22 Sep 2020 02:35:59 +0530

python-avro (1.9.1+dfsg-1) unstable; urgency=medium

  * Team upload.
  * Drop Python2 package (simplify packaging by restricting to Python3 only)
    Closes: #937592
  * Standards-Version: 4.4.1
  * Remove trailing whitespace in debian/changelog
  * debian/copyright: use spaces rather than tabs to start continuation
    lines.
  * Remove obsolete field Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).

 -- Andreas Tille <tille@debian.org>  Wed, 18 Dec 2019 08:51:18 +0100

python-avro (1.9.0+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * debhelper-compat 12
  * Standards-Version: 4.4.0
  * Use secure URI in Homepage field.
  * Set upstream metadata fields: Name.

 -- Andreas Tille <tille@debian.org>  Thu, 01 Aug 2019 22:29:50 +0200

python-avro (1.8.2+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Avoid non-ASCII characters in synopsis
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1
  * Testsuite: autopkgtest-pkg-python
  * Secure URI in copyright format

 -- Andreas Tille <tille@debian.org>  Mon, 29 Oct 2018 13:18:56 +0100

python-avro (1.8.2+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version.

 -- Afif Elghraoui <afif@debian.org>  Fri, 11 Aug 2017 14:50:50 -0400

python-avro (1.8.2~rc1+dfsg-1) experimental; urgency=medium

  * New upstream release candidate
  * Refresh patch
  * Bump copyright years

 -- Afif Elghraoui <afif@debian.org>  Mon, 23 Jan 2017 02:55:15 -0800

python-avro (1.8.1+dfsg-1) unstable; urgency=medium

  * Imported Upstream version 1.8.1+dfsg
  * Refresh patch
  * Bump Standards-Version to 3.9.8

 -- Afif Elghraoui <afif@debian.org>  Sat, 09 Jul 2016 14:21:51 -0700

python-avro (1.8.0+dfsg-2) unstable; urgency=low

  * Rebuild with python 3.5 as default python3 version.
    Closes: #820201
  * Bump standards version.

 -- Afif Elghraoui <afif@debian.org>  Thu, 07 Apr 2016 22:38:55 -0700

python-avro (1.8.0+dfsg-1) unstable; urgency=low

  * Don't install extra license files.
  * Imported Upstream version 1.8.0+dfsg.
  * Fix building with dpkg -A.

 -- Afif Elghraoui <afif@ghraoui.name>  Sun, 07 Feb 2016 15:34:45 -0800

python-avro (1.8.0~rc1+dfsg-2) unstable; urgency=low

  * No changes. The package wasn't built properly on the buildd
  (resulted in empty python3-avro package).

 -- Afif Elghraoui <afif@ghraoui.name>  Sun, 27 Dec 2015 00:07:23 -0800

python-avro (1.8.0~rc1+dfsg-1) unstable; urgency=low

  * Imported Upstream version 1.8.0~rc1+dfsg

 -- Afif Elghraoui <afif@ghraoui.name>  Sat, 26 Dec 2015 23:43:28 -0800

python-avro (1.8.0~rc0+dfsg-2) unstable; urgency=low

  [ Ben Finney ]
  * Clarify package descriptions for Python libraries.
    (Closes: Bug#804200, Bug#804201)

 -- Afif Elghraoui <afif@ghraoui.name>  Sat, 28 Nov 2015 23:02:39 -0800

python-avro (1.8.0~rc0+dfsg-1) unstable; urgency=low

  * Initial release (Closes: #802041)

 -- Afif Elghraoui <afif@ghraoui.name>  Sat, 24 Oct 2015 17:44:23 -0700
